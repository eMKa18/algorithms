package com.codewhatuwant.edu.algorithms.util;

import java.util.Collections;
import java.util.List;

public class ComparableUtils {

    private ComparableUtils() {}

    public static <T> void swap(T[] elements, int first, int second) {
        T firstItem = elements[first];
        elements[first] = elements[second];
        elements[second] = firstItem;
    }

    public static <T> void swap(List<T> elements, int first, int second) {
        Collections.swap(elements, first, second);
    }

    public static <T extends Comparable<T>> boolean less(T first, T second) {
        return first.compareTo(second) < 0;
    }

    public static <T extends Comparable<T>> boolean more(T first, T second) {
        return first.compareTo(second) > 0;
    }

    public static <T extends Comparable<T>> boolean same(T first, T second) {
        return first.compareTo(second) == 0;
    }

}
