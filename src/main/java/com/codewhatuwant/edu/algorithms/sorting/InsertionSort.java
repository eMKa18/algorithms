package com.codewhatuwant.edu.algorithms.sorting;

import com.codewhatuwant.edu.algorithms.util.ComparableUtils;

public class InsertionSort implements Sortable {

    @Override
    public <T extends Comparable<T>> T[] sort(T[] array) {
        for (int index = 1; index < array.length; index++) {
            T key = array[index];
            int lessIndex = index - 1;
            while (lessIndex >= 0 && ComparableUtils.less(key, array[lessIndex])) {
                array[lessIndex + 1] = array[lessIndex];
                lessIndex--;
            }
            array[lessIndex + 1] = key;
        }
        return array;
    }
}
