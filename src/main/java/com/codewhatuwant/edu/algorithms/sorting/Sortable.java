package com.codewhatuwant.edu.algorithms.sorting;

public interface Sortable {
    <T extends Comparable<T>> T[] sort(T[] toBeSorted);
}
