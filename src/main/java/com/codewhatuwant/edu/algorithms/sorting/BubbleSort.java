package com.codewhatuwant.edu.algorithms.sorting;

import com.codewhatuwant.edu.algorithms.util.ComparableUtils;

public class BubbleSort implements Sortable {

    @Override
    public <T extends Comparable<T>> T[] sort(T[] elements) {
        int last = elements.length;
        do {
            for (int index = 0; index < last - 1; index++) {
                if (ComparableUtils.less(elements[index + 1], elements[index])) {
                    ComparableUtils.swap(elements, index, index + 1);
                }
            }
            last--;
        } while (last != 0);
        return elements;
    }

}
