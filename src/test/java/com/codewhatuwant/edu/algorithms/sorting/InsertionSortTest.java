package com.codewhatuwant.edu.algorithms.sorting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InsertionSortTest {

    private InsertionSort insertionSort;

    @BeforeEach
    void setUp() {
        insertionSort = new InsertionSort();
    }

    @Test
    void insertionSortTest() {

        Integer[] given = new Integer[]{0, 5, 9, 2, 1, 3, 4, 8, 6, 7};
        Integer[] expected = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        Integer[] actual = insertionSort.sort(given);

        assertArrayEquals(expected, actual);
    }

    @Test
    void canSortNegativeIntegers() {
        Integer[] given = new Integer[]{-1, -5, -10, -990, 990, 1010};
        Integer[] expected = new Integer[]{-990, -10, -5, -1, 990, 1010};

        Integer[] actual = insertionSort.sort(given);

        assertArrayEquals(expected, actual);
    }

    @Test
    void canSortCharacters() {
        Character[] given = new Character[]{'f', 'h', 'c', 'a', 'b', 'd', 'g', 'e'};
        Character[] expected = new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

        Character[] actual = insertionSort.sort(given);

        assertArrayEquals(expected, actual);
    }
}