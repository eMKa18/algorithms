package com.codewhatuwant.edu.algorithms.sorting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class BubbleSortTest {

    private BubbleSort bubbleSort;

    @BeforeEach
    void setUp() {
        bubbleSort = new BubbleSort();
    }

    @Test
    void canSortIntegers() {
        Integer[] given = new Integer[]{0, 5, 9, 2, 1, 3, 4, 8, 6, 7};
        Integer[] expected = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        Integer[] actual = bubbleSort.sort(given);

        assertArrayEquals(expected, actual);
    }

    @Test
    void canSortNegativeIntegers() {
        Integer[] given = new Integer[]{0, -5, 9, 2, -1, 3, 4, 8, 6, -7};
        Integer[] expected = new Integer[]{-7, -5, -1, 0, 2, 3, 4, 6, 8, 9};

        Integer[] actual = bubbleSort.sort(given);

        assertArrayEquals(expected, actual);
    }

    @Test
    void canSortCharacters() {
        Character[] given = new Character[]{'f', 'h', 'c', 'a', 'b', 'd', 'g', 'e'};
        Character[] expected = new Character[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

        Character[] actual = bubbleSort.sort(given);

        assertArrayEquals(expected, actual);
    }

}